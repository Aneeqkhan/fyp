//
//  CompasViewController.swift
//  LiveCamera
//
//  Created by Apple on 10/05/2019.
//  Copyright © 2019 Apple.inc. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import AVFoundation



class CompasViewController:UIViewController, ARSCNViewDelegate {
    

    @IBOutlet weak var sceneView: ARSCNView!
    var count: Int = 0
    var length:String=""
    var txtRadius : UITextField? = nil
    
    
    var dotNodes = [SCNNode]()
    
    var textNode = SCNNode()
    var lbl:UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set the view's delegate
        sceneView.delegate = self
        
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        //Add Image
       
        
        lbl=UILabel()
        lbl.text="Angle is....."
        lbl.textAlignment = .center
        lbl.frame = CGRect( x:200, y:50, width:250, height: 80)
        self.view.addSubview(lbl)
        
        
    }
    
    func playvideoalert() {
        
        // var obj = Global()
        
        let alert = UIAlertController(title: "Choose", message: "do you want see tutorial", preferredStyle: UIAlertController.Style.alert)
        
        
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) in
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "playVideoViewController") as! playVideoViewController
            self.present(newViewController, animated: true, completion: nil)
            newViewController.condition = 3
            
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler:nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let configuration = ARWorldTrackingConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
       
    }
    func generateRuntimeLabel(point:CGPoint,type:Int){
        let lbl=UILabel()
        lbl.text=String(type)
        //lbl.textAlignment = .center
        lbl.frame = CGRect( x:point.x, y:point.y, width:250, height: 80)
        self.view.addSubview(lbl)
    }
    var firstPoint:CGPoint = CGPoint()
    var secondPoint:CGPoint = CGPoint()
    var thirdPoint:CGPoint = CGPoint()
    var fourthPoint:CGPoint = CGPoint()
    var fifthPoint:CGPoint = CGPoint()
    var sixthPoint:CGPoint = CGPoint()
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        count = count + 1
//        if dotNodes.count >= 3 {
        
//            for dot in dotNodes {
//                dot.removeFromParentNode()
//            }
//
//            dotNodes = [SCNNode]()
//        }

        if let touch = touches.first {
            let touchLocation = touch.location(in: sceneView)
            print("count: ",count)
            if(count==1)
            {
                firstPoint=touchLocation
                addDotAtView(location: touchLocation)
                getRadiusALert()
                generateRuntimeLabel(point: firstPoint, type: 1)
            }
           else if(count==2)
            {
                secondPoint = touchLocation
                addDotAtView(location: touchLocation)
                generateRuntimeLabel(point: secondPoint, type: 2)
                drawDottedLine(start: firstPoint, end: secondPoint, view: self.view)
            }
            else if(count == 3)
            {
                let distance=CGPointDistance(from: firstPoint, to: secondPoint)
                //var angle:Float = getAngle(point1: firstPoint, point2: touchLocation)
                //print("angle",angle)
                //get Required point from angle and radius
                //let endPoint = CGPoint(x: firstPoint.x + sin(CGFloat(angle)) * distance,
                                      // y: firstPoint.y + cos(CGFloat(angle)) * distance)
                //addLine(fromPoint: firstpoint, toPoint: endPoint)
                //addDotAtView(location: endPoint)
                //addLine(fromPoint: secondPoint, toPoint: endPoint)
               
               
                
                
                thirdPoint = touchLocation
                addDotAtView(location: touchLocation)
               
                 drawDottedLine(start: firstPoint, end: thirdPoint, view: self.view)
            
                generateSpecialCurve(from: secondPoint, to: thirdPoint, bendFactor: -0.25, thickness: 10.0)
                generateRuntimeLabel(point: thirdPoint, type: 3)
                
                
                
        
//                //draw curve
//                var pointsArray:[CGPoint] = [CGPoint]()
//                pointsArray.append(secondPoint)
//                pointsArray.append(thirdPoint)
//                var path=quadCurvedPathWithPoint(points: pointsArray)
//                let shapeLayer = CAShapeLayer()
//                shapeLayer.path = path.cgPath
//
//                //change the fill color
//                shapeLayer.fillColor = UIColor.clear.cgColor
//                //you can change the stroke color
//                shapeLayer.strokeColor = UIColor.red.cgColor
//                //you can change the line width
//                shapeLayer.lineWidth = 3.0
//
//                sceneView.layer.addSublayer(shapeLayer)
                
            
            }
            else if(count==4)
            {
                fourthPoint=touchLocation
                addDotAtView(location: touchLocation)
                getRadiusALert()
                generateRuntimeLabel(point: fourthPoint, type: 4)
            }
            else if(count==5)
            {
                fifthPoint=touchLocation
                addDotAtView(location: touchLocation)
                generateRuntimeLabel(point: fifthPoint, type: 5)
                drawDottedLine(start: fourthPoint, end: fifthPoint, view: self.view)
            }
            else if(count==6)
            {
                sixthPoint=touchLocation
                addDotAtView(location: touchLocation)
                generateSpecialCurve(from: fifthPoint, to: sixthPoint, bendFactor: -0.25, thickness: 10.0)
                //addLine(fromPoint: fifthPoint, toPoint: sixthPoint)
                //drawTriangleusingSixthPoint()
                generateRuntimeLabel(point: sixthPoint, type: 6)
                drawDottedLine(start: fourthPoint, end: sixthPoint, view: self.view)
            }
        }
    }
    
    func generateSpecialCurve(from: CGPoint, to: CGPoint, bendFactor: CGFloat, thickness: CGFloat){
        
        let center = CGPoint(x: (from.x+to.x)*0.5, y: (from.y+to.y)*0.5)
        let normal = CGPoint(x: -(from.y-to.y), y: (from.x-to.x))
        let normalNormalized: CGPoint = {
            let normalSize = sqrt(normal.x*normal.x + normal.y*normal.y)
            guard normalSize > 0.0 else { return .zero }
            return CGPoint(x: normal.x/normalSize, y: normal.y/normalSize)
        }()
        
        let path = UIBezierPath()
        
        path.move(to: from)
        
        let midControlPoint: CGPoint = CGPoint(x: center.x + normal.x*bendFactor, y: center.y + normal.y*bendFactor)
        let closeControlPoint: CGPoint = CGPoint(x: midControlPoint.x + normalNormalized.x*thickness*0.5, y: midControlPoint.y + normalNormalized.y*thickness*0.5)
        let farControlPoint: CGPoint = CGPoint(x: midControlPoint.x - normalNormalized.x*thickness*0.5, y: midControlPoint.y - normalNormalized.y*thickness*0.5)
       
        
        path.addQuadCurve(to: to, controlPoint: closeControlPoint)
        path.addQuadCurve(to: from, controlPoint: farControlPoint)
    
        
        let line = CAShapeLayer()
        
        line.path = path.cgPath
        
        
        
        line.strokeColor = UIColor.red.cgColor
        
        line.lineWidth = 1
        
        line.lineJoin = CAShapeLayerLineJoin.round
        
        self.view.layer.addSublayer(line)
        
    }
    
    
    func drawTriangleusingSixthPoint(){
        var intersectionPoint=getIntersectionOfLines(line1: (a: secondPoint, b: thirdPoint), line2: (a: fifthPoint, b: sixthPoint))
        addLine(fromPoint: firstPoint, toPoint: intersectionPoint)
        addLine(fromPoint: fourthPoint, toPoint: intersectionPoint)
        addLine(fromPoint: firstPoint, toPoint: fourthPoint)
    }
        //Fine distance between two points
    func CGPointDistance(from: CGPoint, to: CGPoint) -> CGFloat {
        return sqrt(CGPointDistanceSquared(from: from, to: to))
    }
    func getIntersectionOfLines(line1: (a: CGPoint, b: CGPoint), line2: (a: CGPoint, b: CGPoint)) -> CGPoint {
        
        let distance = (line1.b.x - line1.a.x) * (line2.b.y - line2.a.y) - (line1.b.y - line1.a.y) * (line2.b.x - line2.a.x)
        
        let u = ((line2.a.x - line1.a.x) * (line2.b.y - line2.a.y) - (line2.a.y - line1.a.y) * (line2.b.x - line2.a.x)) / distance
        let v = ((line2.a.x - line1.a.x) * (line1.b.y - line1.a.y) - (line2.a.y - line1.a.y) * (line1.b.x - line1.a.x)) / distance
        
        
        
        return CGPoint(x: line1.a.x + u * (line1.b.x - line1.a.x), y: line1.a.y + u * (line1.b.y - line1.a.y))
    }
    //get Squared distance
    
    func CGPointDistanceSquared(from: CGPoint, to: CGPoint) -> CGFloat {
        
        
        
        return (from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y)
        
        
        
    }
    
    
    
    
    
    //get Angle
    
    func getAngle(point1:CGPoint,point2:CGPoint)->Float{
        let originX = point1.x - point2.x
        let originY = point1.y - point2.y
        let bearingRadians = atan2f(Float(originY), Float(originX))
        return bearingRadians
    }
    //add dot
    func addDotAtView(location:CGPoint){
        let label = UILabel(frame: CGRect(x: location.x, y: location.y, width: self.view.frame.size.width, height: 50))
        label.textAlignment = .center
        label.text = "*"
        
        //To set the color
        label.textColor = UIColor.red
        
        //To set the font Dynamic
        label.font = UIFont(name:"Helvetica-Regular", size: 40.0)
        //To set the system font
        label.font = UIFont.systemFont(ofSize: 30.0)
        
        //To display multiple lines
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping //Wrap the word of label
        label.lineBreakMode = .byCharWrapping //Wrap the charactor of label
        
        label.sizeToFit()
        self.sceneView.addSubview(label)
    }
    
    //add Line
    func addLineAtView(fromPoint start: CGPoint, toPoint end:CGPoint) {
        
        let line = CAShapeLayer()
        
        let linePath = UIBezierPath()
        
        linePath.move(to: start)
        
        linePath.addLine(to: end)
        linePath.addQuadCurve(to: start, controlPoint: end)
        line.path = linePath.cgPath
        
        
        
        line.strokeColor = UIColor.green.cgColor
        
       // line.lineWidth = 1
        
        let  dashes: [ CGFloat ] = [ 0.0, 16.0 ]
        linePath.setLineDash(dashes, count: dashes.count, phase: 0.0)
        line.lineJoin = CAShapeLayerLineJoin.round
        
        
        self.sceneView.layer.addSublayer(line)
        
    }
   
    
    @IBAction func Help(_ sender: Any) {
        playvideoalert()
    }
    
    
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.green.cgColor
        shapeLayer.lineWidth = 3
        shapeLayer.lineDashPattern = [7, 3] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    
    func quadCurvedPathWithPoint(points: [CGPoint] ) -> UIBezierPath {
        let path = UIBezierPath()
        if points.count > 1 {
            var prevPoint:CGPoint?
            for (index, point) in points.enumerated() {
                if index == 0 {
                    path.move(to: point)
                } else {
                    if index == 1 {
                        path.addLine(to: point)
                    }
                    if prevPoint != nil {
                        let midPoint = self.midPointForPoints(from: prevPoint!, to: point)
                        path.addQuadCurve(to: midPoint, controlPoint: controlPointForPoints(from: midPoint, to: prevPoint!))
                        path.addQuadCurve(to: point, controlPoint: controlPointForPoints(from: midPoint, to: point))
                    }
                }
                prevPoint = point
            }
        }
        return path
    }
    
    func midPointForPoints(from p1:CGPoint, to p2: CGPoint) -> CGPoint {
        return CGPoint(x: (p1.x + p2.x) / 2, y: (p1.y + p2.y) / 2)
    }
    
    func controlPointForPoints(from p1:CGPoint,to p2:CGPoint) -> CGPoint {
        var controlPoint = midPointForPoints(from:p1, to: p2)
        let  diffY = abs(p2.y - controlPoint.y)
        if p1.y < p2.y {
            controlPoint.y = controlPoint.y + diffY
        } else if ( p1.y > p2.y ) {
            controlPoint.y = controlPoint.y - diffY
        }
        return controlPoint
    }
    func drawCircle(point:CGPoint,radius:CGFloat){
        let circlePath = UIBezierPath(arcCenter: CGPoint(x:point.x,y: point.y), radius: radius, startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayer.strokeColor = UIColor.red.cgColor
        //you can change the line width
        shapeLayer.lineWidth = 3.0
        
        sceneView.layer.addSublayer(shapeLayer)
    }

    func addDot(at hitResult: ARHitTestResult) {
        let dotGeometry = SCNSphere(radius: 0.01)
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.red
        dotGeometry.materials = [material]
        
        
            let dotNode = SCNNode(geometry: dotGeometry)
            dotNode.position = SCNVector3(
                x: hitResult.worldTransform.columns.3.x,
                y: hitResult.worldTransform.columns.3.y,
                z: hitResult.worldTransform.columns.3.z
            )
            
            sceneView.scene.rootNode.addChildNode(dotNode)
            dotNodes.append(dotNode)
            
                   //ask for radius
        if(dotNodes.count==1)
        {
    getRadiusALert()
        }

        
    }
    
    
    @IBAction func ScreenShot(_ sender: Any) {
        var capImage = takeScreenshot()
        
        showalertview()
    }
    func takeScreenshot(_ shouldSave: Bool = true)  {
        
        var firstImage = sceneView.snapshot()
        UIImageWriteToSavedPhotosAlbum(firstImage, nil, nil, nil)
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        let context = UIGraphicsGetCurrentContext();
        layer.render(in:context!)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let image = screenshotImage, shouldSave {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        //        return screenshotImage
    }
    
    
    func showalertview() {
        
        let alert = UIAlertController(title: "ScreenShot", message: "Screenshot Captured", preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //Handler
    
    func configurationTextField(textField: UITextField!) {
        
        if (textField) != nil {
            
            self.txtRadius = textField!        //Save reference to the UITextField
            
            self.txtRadius!.placeholder = "Length in CM";
            
        }
        
    }
    
    
    
    
    
    
    func getRadiusALert() {
        
        let alert = UIAlertController(title: "Enter Length", message: "Length is required", preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addTextField(configurationHandler:configurationTextField )
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            
            print(self.txtRadius?.text)
            
                
            
            
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

   
    func addLine(fromPoint start: CGPoint, toPoint end:CGPoint) {
        
        let line = CAShapeLayer()
        
        let linePath = UIBezierPath()
        
        linePath.move(to: start)
        
        linePath.addLine(to: end)
        
        line.path = linePath.cgPath
        
        
        
        line.strokeColor = UIColor.red.cgColor
        
        line.lineWidth = 1
        
        line.lineJoin = CAShapeLayerLineJoin.round
        
        self.view.layer.addSublayer(line)
        
    }
    
    
    
}


