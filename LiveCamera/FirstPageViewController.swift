//
//  FirstPageViewController.swift
//  LiveCamera
//
//  Created by Apple on 15/04/2019.
//  Copyright © 2019 Apple.inc. All rights reserved.
//

import UIKit

class FirstPageViewController: UIViewController {

    @IBOutlet weak var btnprotector: UIButton!
    
    @IBOutlet weak var btnScaling: UIButton!
    
    @IBOutlet weak var btnCompass: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        btnprotector.layer.borderWidth = 1.5
        btnprotector.layer.cornerRadius = 18
        
        btnScaling.layer.borderWidth = 2.5
        btnScaling.layer.cornerRadius = 18
        
        btnCompass.layer.borderWidth = 1.5
        btnCompass.layer.cornerRadius = 18
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
