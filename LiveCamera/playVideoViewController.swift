//
//  playVideoViewController.swift
//  LiveCamera
//
//  Created by Apple on 29/06/2019.
//  Copyright © 2019 Apple.inc. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class playVideoViewController: UIViewController {

    
    var condition:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(condition==1)
        {
            playVideoforprotector()
            
        }
        else if(condition==2){
               playVideoforscaling()
        }
        else if(condition==3){
            
               playVideoforcompass()
        }
        
    }
    func playVideoforprotector() {
        guard let path = Bundle.main.path(forResource: "Protector", ofType:"mp4") else {
            debugPrint("video.mp4 not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }

    func playVideoforcompass() {
        guard let path = Bundle.main.path(forResource: "compassdemo", ofType:"mp4") else {
            debugPrint("video.mp4 not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    func playVideoforscaling() {
        guard let path = Bundle.main.path(forResource: "Scaling", ofType:"mp4") else {
            debugPrint("video.mp4 not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }

}
