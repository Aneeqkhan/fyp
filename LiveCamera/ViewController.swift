//
//  ViewController.swift
//  LiveCamera
//
//  Created by Apple on 25/03/2019.
//  Copyright © 2019 Apple.inc. All rights reserved.
//

import UIKit
import SpriteKit
import ARKit
import SceneKit
import AVFoundation
import AVKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, ARSCNViewDelegate{
    
    
    @IBOutlet weak var btnAnticlockwise: UIButton!
    @IBOutlet weak var btnClockwise: UIButton!
    var zoomFactor : Bool = true
    @IBAction func clockwise(_ sender: Any) {
        
        let imageFromButton : UIImage = btnClockwise.image(for: UIControl.State.normal)!
        if(imageFromButton==#imageLiteral(resourceName: "off"))
        {
            checkleftsidebtn()
        }
        else
       {
            checkrightsidebtn()
        
    }
    
   
    }
    func checkrightsidebtn() {
        
        btnAnticlockwise.setImage(#imageLiteral(resourceName: "On"), for: .normal)
        btnClockwise.setImage(#imageLiteral(resourceName: "off"), for: .normal)
        
        
    }
    func checkleftsidebtn(){
        
        btnClockwise.setImage(#imageLiteral(resourceName: "On"), for: .normal)
        btnAnticlockwise.setImage(#imageLiteral(resourceName: "off"), for: .normal)
    }
    
    
    @IBAction func antiClockwiseClick(_ sender: Any) {
    //getanti clock wise button imageFrombutton
        let imageFromButton : UIImage = btnAnticlockwise.image(for: UIControl.State.normal)!
        if(imageFromButton==#imageLiteral(resourceName: "off"))
        {
         checkrightsidebtn()
        }
        else
        {
            checkleftsidebtn()
        }
        
    }
    
    @IBOutlet weak var sceneView: ARSCNView!
    var dotNodes = [SCNNode]()
    
    var textNode = SCNNode()
    
    var lbl:UILabel!
    var count:Int = 0
    var firstpoint: CGPoint = CGPoint()
    var secondpoint: CGPoint = CGPoint()
    var clockBit: Bool = Bool()
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        sceneView.delegate = self

        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]


        addImage()
//hide buton clockwise and anticlockwise on didload
        btnClockwise.isHidden=true;
        btnAnticlockwise.isHidden=true;
        playvideoalert()
       // openAlertView()
    }
    
    
    
    
    func addImage() {
        let node = SCNNode(geometry: SCNSphere(radius: 0.01))
        //node.geometry?.materials.first?.diffuse.contents = color
        //        node.physicsBody? = .static()
        //        node.name = name
        //        node.physicsBody?.categoryBitMask = MaskNum.barrier.rawValue
        node.geometry?.materials.first?.diffuse.contents = UIImage(named: "Protactor.png")
        node.position = SCNVector3(0.0, 0.0, -5.0)
        sceneView.pointOfView?.addChildNode(node)
    }
    
    @IBAction func Protec(_ sender: Any) {
        zoomFactor=false
        openAlertView()
        //playvideoalert()
        show()
        
    }
    
    
    @IBAction func help(_ sender: Any) {
        playvideoalert()
    }
    
    
    func playvideoalert() {
        
        // var obj = Global()
        
        let alert = UIAlertController(title: "Choose", message: "do you want see tutorial", preferredStyle: UIAlertController.Style.alert)
        
        
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) in
            //move from oneview to another
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "playVideoViewController") as! playVideoViewController
            newViewController.condition = 1
            self.present(newViewController, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler:nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let configuration = ARWorldTrackingConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
          sceneView.session.pause()
    }
    

    
   

    @IBAction func didTakePhoto(_ sender: Any) {
        
       var capImage = takeScreenshot()
    
      showalertview()
    }
    
    
    
    func openAlertView() {
        
       // var obj = Global()
        
        let alert = UIAlertController(title: "Choose", message: "Measure clockwise or Anticlockwise", preferredStyle: UIAlertController.Style.alert)
        
       
        
        alert.addAction(UIAlertAction(title: "Left side", style: .default, handler: { (UIAlertAction) in
            
            self.clockBit = true
            self.checkleftsidebtn()
            
        }))
        alert.addAction(UIAlertAction(title: "Right Side", style: .default, handler:{ (UIAlertAction) in
           self.checkrightsidebtn()
            self.clockBit = false
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    
    @IBAction func Reload(_ sender: Any) {
        
      loadView()
    }

    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if dotNodes.count >= 2 {
//            for dot in dotNodes {
//                dot.removeFromParentNode()
//            }
//
//            dotNodes = [SCNNode]()
//        }
//
//        if let touch = touches.first {
//            let touchLocation = touch.location(in: sceneView)
//            let results = sceneView.hitTest(touchLocation, types: .featurePoint)
//
//            if let hitResult = results.first {
//                addDot(at: hitResult)
//            }
//        }
//    }
    
    func addDot(at hitResult: ARHitTestResult) {
        let dotGeometry = SCNSphere(radius: 0.01)
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.red
        dotGeometry.materials = [material]
        let dotNode = SCNNode(geometry: dotGeometry)
        dotNode.position = SCNVector3(
            x: hitResult.worldTransform.columns.3.x,
            y: hitResult.worldTransform.columns.3.y,
            z: hitResult.worldTransform.columns.3.z
        )
        
        sceneView.scene.rootNode.addChildNode(dotNode)
        
        dotNodes.append(dotNode)
        if dotNodes.count >= 2 {
            calculate()
        }
    }
    func calculate() {
        let start = dotNodes[0]
        let end = dotNodes[1]
        
        
        let a = end.position.x - start.position.x
        let b = end.position.y - start.position.y
        let c = end.position.z - start.position.z
        
        let distance = sqrt(pow(a, 2) + pow(b, 2) + pow(c, 2))
    }
    
   override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       if(zoomFactor==false)
       {
        if let touch = touches.first {
            
            let location = touch.location(in: sceneView)
            
            
            var x1 = location.x
            var y1 = location.y
            //            var dx=Double(x1)
            //            var dy=Double(x1)
            //            var x = Int(x1)
            //            var y = Int(y1)
            //            var xstring = String(x)
            //            var ystring = String(y)
            
            
            print(location)
            //            let first: CGPoint = CGPoint(x:43,y:281)
            //            let second: CGPoint = CGPoint(x:265,y:281)
            //            let third: CGPoint = CGPoint(x:x1,y:y1)
            
            
            let newPoint: CGPoint = CGPoint(x: x1, y: y1)
            if (clockBit == true) {
                Clockwise(point: newPoint)
            }
            else
            {
                
                
                AntiClockwise(point: newPoint)
            }
            
            
            
            //
            //           // below side protector measure
            //
            ////            let Centre: CGPoint = CGPoint(x:140.0,y:338.5)
            ////            addDot(location: Centre)
            ////            let Corner: CGPoint = CGPoint(x:36,y:338.5)
            ////            addDot(location: Corner)
            ////            let NewPoint: CGPoint = CGPoint(x:x1,y:y1)
            ////            addDot(location: NewPoint)
            ////            angleBetweenThreePoints(center: Centre, firstPoint: Corner, secondPoint: NewPoint)
            ////            addLine(fromPoint: Centre, toPoint: NewPoint)
            //
            //
            //
            //
            ////            let four: CGPoint = CGPoint(x: 140.0, y: 338.5)
            ////
            ////            var xDistA:CGFloat = (second.x - third.x);
            ////            var yDistA:CGFloat = (second.y - third.y);
            ////            var a:CGFloat = sqrt((xDistA * xDistA) + (yDistA * yDistA));
            ////
            ////            var xDistB:CGFloat = (first.x - third.x);
            ////            var yDistB:CGFloat = (first.y - third.y);
            ////            var b:CGFloat = sqrt((xDistB * xDistB) + (yDistB * yDistB));
            ////
            ////            var xDistC:CGFloat = (second.x - first.x);
            ////            var yDistC:CGFloat = (second.y - first.y);
            ////            var c:CGFloat = sqrt((xDistC * xDistC) + (yDistC * yDistC));
            ////
            ////            var angle:CGFloat = acos(((a*a)+(b*b)-(c*c))/((2*(a)*(b))));
            ////
            ////           print("Angle is ",angle)
            ////            addLine(fromPoint: four, toPoint: third)
       }
    
 }
//
      }
//
    
    // all functions
    
    @objc func zoom(gesture: UIPinchGestureRecognizer) {
        let view = self.view as! SCNView
        let node = view.scene!.rootNode.childNode(withName: "Camera", recursively: false)
        let scale = gesture.velocity
        
        switch gesture.state {
        case .began:
            break
        case .changed:
            node!.camera!.fieldOfView = node!.camera!.fieldOfView - CGFloat(scale)
            print(node!.camera!.fieldOfView)
            break
        default: break
        }
    }
    
    
    func show()  {
        
        //openAlertView()
        
        lbl=UILabel()
        lbl.text="Angle is....."
        lbl.textAlignment = .center
        lbl.frame = CGRect( x:200, y:50, width:250, height: 80)
        self.view.addSubview(lbl)
        
        
        var imageView : UIImageView
        imageView  = UIImageView(frame:CGRect(x:-8, y:250, width:430, height:250));
        imageView.image = UIImage(named:"Protactor.png")
        
        self.sceneView.addSubview(imageView)
        
        btnClockwise.isHidden=false;
        btnAnticlockwise.isHidden=false;
        //for label
       
        
    }
    
    
    
    func angleBetweenThreePoints(center: CGPoint, firstPoint: CGPoint, secondPoint: CGPoint) -> CGFloat {
        let firstAngle = atan2(firstPoint.y - center.y, firstPoint.x - center.x)
        let secondAnlge = atan2(secondPoint.y - center.y, secondPoint.x - center.x)
        var angleDiff = firstAngle - secondAnlge
        
        if angleDiff < 0 {
            angleDiff *= -1
        }
        var angle2 = angleDiff * 180 / .pi
        
        let t:Float = Float(angle2)
        let w:Int = Int(t)
        let v:String = String(w)
        lbl.text = v+"  "+("Degree")
        return angleDiff
    }
    
    
    func angleBetweenThreePointsclockwise(center: CGPoint, firstPoint: CGPoint, secondPoint: CGPoint) -> CGFloat {
        let firstAngle = atan2(firstPoint.y - center.y, firstPoint.x - center.x)
        let secondAnlge = atan2(secondPoint.y - center.y, secondPoint.x - center.x)
        var angleDiff = firstAngle - secondAnlge
        
        if angleDiff < 0 {
            angleDiff *= -1
        }
        var angle2 = 90-angleDiff * (180) / .pi
        
        
        let t:Float = Float(angle2)
        let w:Int = Int(t)
        let v:String = String(w)
        lbl.text = v+"  "+("Degree")
        return angleDiff
    }
    
    
    
    func takeScreenshot(_ shouldSave: Bool = true)  {
        
        var firstImage = sceneView.snapshot()
            UIImageWriteToSavedPhotosAlbum(firstImage, nil, nil, nil)
      var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        let context = UIGraphicsGetCurrentContext();
        layer.render(in:context!)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let image = screenshotImage, shouldSave {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
//        return screenshotImage
    }
    
    
    
    func addDot(location:CGPoint){
        let label = UILabel(frame: CGRect(x: location.x, y: location.y, width: self.view.frame.size.width, height: 50))
        label.textAlignment = .center
        label.text = "."
        
        //To set the color
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.red
        
        //To set the font Dynamic
        label.font = UIFont(name: "Helvetica-Regular", size: 20.0)
        
        //To set the system font
        label.font = UIFont.systemFont(ofSize: 20.0)
        
        //To display multiple lines
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping //Wrap the word of label
        label.lineBreakMode = .byCharWrapping //Wrap the charactor of label
        
        label.sizeToFit()
        self.view.addSubview(label)
    }
    
    
    func addLine(fromPoint start: CGPoint, toPoint end:CGPoint) {
        
        let line = CAShapeLayer()
        
        let linePath = UIBezierPath()
        
        linePath.move(to: start)
        
        linePath.addLine(to: end)
        
        line.path = linePath.cgPath
        
        line.strokeColor = UIColor.red.cgColor
        
        line.lineWidth = 1
        
        line.lineJoin = CAShapeLayerLineJoin.round
        
        self.view.layer.addSublayer(line)
        
    }
    
    func Clockwise(point: CGPoint){
        if(point.y <= 484)
        {
            
            
            let centre: CGPoint = CGPoint(x:207.0,y:484.0)
            //addDot(location: centre)
            let corner: CGPoint = CGPoint(x:201.0,y:484.0)
            // addDot(location: corner)
            let newPoint: CGPoint = CGPoint(x:point.x,y:point.y)
            
            angleBetweenThreePoints(center: centre, firstPoint: corner, secondPoint: newPoint)
            addLine(fromPoint: centre, toPoint: newPoint)
            
        }
        
        
        
        
    }
    
    func AntiClockwise(point: CGPoint) {
        if(point.y<=484)
        {
            let centre: CGPoint = CGPoint(x:207.0,y:484.0)
            //addDot(location: centre)
            let corner: CGPoint = CGPoint(x:409.0,y:484.0)
            // addDot(location: corner)
            let newPoint: CGPoint = CGPoint(x:point.x,y:point.y)
            
            angleBetweenThreePoints(center: centre, firstPoint: corner, secondPoint: newPoint)
            addLine(fromPoint: centre, toPoint: newPoint)
        }
        
        
        
    }
    
    
    func showalertview(){
        
        let alert = UIAlertController(title: "ScreenShot", message: "Screenshot Captured", preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
    
    
        }

    



