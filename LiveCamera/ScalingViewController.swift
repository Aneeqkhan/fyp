 
 
 
 //
 //  ViewController.swift
 //  AR Ruler
 //
 //  Created by Eileen Zhong on 7/21/18.
 //  Copyright © 2018 Eileen Zhong. All rights reserved.
 //
 
 import UIKit
 import SceneKit
 import ARKit
 import ReplayKit
 
 class ScalingViewController: UIViewController, ARSCNViewDelegate {
    
    @IBOutlet var sceneView: ARSCNView!
    
    @IBOutlet weak var CM: UILabel!
    @IBOutlet weak var Inches: UILabel!
    @IBOutlet weak var feet: UILabel!
    
    @IBAction func startRecording(_ sender: Any) {
        
        let recorder = RPScreenRecorder.shared()
        
        recorder.startRecording{ [unowned self] (error) in
            if let unwrappedError = error {
                print(unwrappedError.localizedDescription)
            } else {
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Stop", style: .plain, target: self, action: #selector(self.stopRecording))
            }
        }
    }
    
    
    
    
    @IBAction func Help(_ sender: Any) {
        playvideoalert()
    }
    
    
    @IBAction func stopRecording(_ sender: Any) {
        
        
        let recorder = RPScreenRecorder.shared()
        
        recorder.stopRecording { [unowned self] (preview, error) in
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Start", style: .plain, target: self, action: #selector(self.startRecording))
            
            if let unwrappedPreview = preview {
                unwrappedPreview.previewControllerDelegate = self as? RPPreviewViewControllerDelegate
                self.present(unwrappedPreview, animated: true)
            }
        }
    }
    
    func generateLabel(name:String, xIndex:Int,yIndex:Int)  {
       
        var lbl: UILabel = UILabel()
        lbl.text = name
        lbl.textColor = UIColor.red
        //lbl.font = UIFont(name: "Helvetica-Regular" , size: 40.0)
        lbl.font = UIFont.systemFont(ofSize: 20.0)
        
        
        lbl.frame = CGRect( x:xIndex, y:yIndex, width:250, height: 80)
        self.view.addSubview(lbl)
        
    }
    
    
    @IBAction func Reload(_ sender: Any) {
        loadView()
    }
    
    
    @IBAction func didTakephoto(_ sender: Any) {
        var CapImage = takeScreenshot()
        showalertview()
    }

    
    
    func showalertview(){
        
        let alert = UIAlertController(title: "ScreenShot", message: "Screenshot Captured", preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
    
    
    func takeScreenshot(_ shouldSave: Bool = true) -> UIImage? {
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        guard let context = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let image = screenshotImage, shouldSave {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        return screenshotImage
    }
    
    
    var dotNodes = [SCNNode]()
    
    var textNode = SCNNode()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        //Add Image
        CM.isHidden = true
        feet.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    
    func playvideoalert() {
        
        // var obj = Global()
        
        let alert = UIAlertController(title: "Choose", message: "do you want see tutorial", preferredStyle: UIAlertController.Style.alert)
        
        
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) in
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "playVideoViewController") as! playVideoViewController
            self.present(newViewController, animated: true, completion: nil)
            newViewController.condition = 2
            
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler:nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if dotNodes.count >= 2 {
            
            for dot in dotNodes {
                dot.removeFromParentNode()
            }
            //this below line hide two points when click of 3rd point
            dotNodes = [SCNNode]()
        }
        
        
        
        
        
        if let touch = touches.first {
            let touchLocation = touch.location(in: sceneView)
            let results = sceneView.hitTest(touchLocation, types: .featurePoint)
            
            if let hitResult = results.first {
                addDot(at: hitResult)
            }
        }
    }
    
    func addDot(at hitResult: ARHitTestResult) {
        let dotGeometry = SCNSphere(radius: 0.01)
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.red
        dotGeometry.materials = [material]
        let dotNode = SCNNode(geometry: dotGeometry)
        dotNode.position = SCNVector3(
            x: hitResult.worldTransform.columns.3.x,
            y: hitResult.worldTransform.columns.3.y,
            z: hitResult.worldTransform.columns.3.z
        )
        
        sceneView.scene.rootNode.addChildNode(dotNode)
        
        dotNodes.append(dotNode)
        if dotNodes.count >= 2 {
            calculate()
        }
    }
    
    func metersToInches(fl:Float) -> Float {
        return fl * 39.3701
    }
    func metersToCM(fl:Float) -> Float {
        return fl * 100
    }
    func metersTofoot(fl:Float) -> Float {
        return fl * 3.28084
    }
    func calculate() {
        let start = dotNodes[0]
        let end = dotNodes[1]
        
        let a = end.position.x - start.position.x
        let b = end.position.y - start.position.y
        let c = end.position.z - start.position.z
        
        let distance = sqrt(pow(a, 2) + pow(b, 2) + pow(c, 2))
        
        
        let inches = metersToInches(fl: distance)
       // generateLabel(name: "Inches: "+String(inches), xIndex: 200, yIndex: 50)
        let split = String(format: "%.2f", inches)
         Inches.text = "Inches: "+split
        
        
        let cm = metersToCM(fl: distance)
        //generateLabel(name: "Cm: "+String(cm), xIndex: 200, yIndex: 25)
        let splitt = String(format: "%.2f", cm)
        CM.text = "CM: "+splitt
        
        
        
        let Feet = metersTofoot(fl: distance)
        //generateLabel(name: "feet:  "+String(foot), xIndex: 200, yIndex: 75)
        let splittt = String(format: "%.2f", Feet)
        feet.text = "feet: "+splittt
        
        
        //updateText(text: "\(abs(distance))", atPosition: end.position)
    }
    
    func updateText(text: String, atPosition position: SCNVector3) {
        textNode.removeFromParentNode()
        
        let textGeometry = SCNText(string: text, extrusionDepth: 1.0)
        textGeometry.firstMaterial?.diffuse.contents = UIColor.red
        
        textNode = SCNNode(geometry: textGeometry)
        
        textNode.position = SCNVector3(position.x, position.y + 0.01, position.z)
        
        textNode.scale = SCNVector3(0.01, 0.01, 0.01)
        
        if let camera = sceneView.pointOfView {
            textNode.orientation = camera.orientation
        }
        
        sceneView.scene.rootNode.addChildNode(textNode)
    }
    
 }


